FROM registry.gitlab.ics.muni.cz:443/2780/mysql
MAINTAINER  (me) <email>

# Copy the database schema to the /data directory
COPY files/run_db files/init_db /tmp/
RUN chmod +x /tmp/run_db && chmod +x /tmp/init_db
ADD http://dior.ics.muni.cz/~cuda/seed-cbioportal_hg19_v2.4.0.sql /tmp/
ADD http://dior.ics.muni.cz/~cuda/cgds.sql /tmp/

# init_db will create the default
# database from epcis_schema.sql, then
# stop mysqld, and finally copy the /var/lib/mysql directory
# to default_mysql_db.tar.gz
RUN /tmp/init_db

# run_db starts mysqld, but first it checks
# to see if the /var/lib/mysql directory is empty, if
# it is it is seeded with default_mysql_db.tar.gz before
# the mysql is fired up

ENTRYPOINT "/tmp/run_db"

